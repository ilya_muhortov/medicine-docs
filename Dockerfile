FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    vim \
    gettext

WORKDIR /app

RUN mkdir /docs

ADD requirements.txt ./

RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt

COPY source/ ./source/
COPY Makefile uwsgi.ini ./

RUN make html
