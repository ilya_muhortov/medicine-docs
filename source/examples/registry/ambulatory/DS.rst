Дневной стационар
=================

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Contents:

Для создания талона дневного стационара, необходимо в амбулаторном талоне указать условия  ``2 - В дневном стационаре``

    .. image:: /_static/examples/registry/ambulatory/ds-1.png 

------

.. image:: /_static/examples/registry/ambulatory/ds-2.gif 