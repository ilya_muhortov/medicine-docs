Талоны амбулаторных пациентов
=============================

.. toctree::
    :maxdepth: 2
    :caption: Contents:
   
    overview
    add
    DS

На печатной форме талона присутствует уникальный номер талон. При создании нового талона следует ввести этот номер на нажать на клавиатуре ``Enter``  

.. figure:: /_static/examples/registry/ambulatory/index-1.png
    :align: center
    
    Печатная форма талона