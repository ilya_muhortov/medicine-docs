Талоны амбулаторных пациентов
=============================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    
    ambulatory/index
    dispensary/index
    dental/index
    prophylactic/index

На печатной форме талона присутствует уникальный номер талон. При создании нового талона следует ввести этот номер на нажать на клавиатуре "Enter"  
